package com.example.appgym.ui.register;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.appgym.Constant;
import com.example.appgym.R;
import com.example.appgym.ui.login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText txtNombres, txtApellidos, txtCelular, txtCorreo, txtPassword;
    Button btnRegistro, btnIniciarSession;

    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;
    ProgressBar loadingProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txtNombres = findViewById(R.id.txtNombres);
        txtApellidos = findViewById(R.id.txtApellidos);
        txtCelular = findViewById(R.id.txtCelular);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtPassword = findViewById(R.id.txtPassword);
        btnRegistro = findViewById(R.id.btnRegistrarse);
        btnIniciarSession = findViewById(R.id.btnIniciarSession);
        loadingProgressBar = findViewById(R.id.loading);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombres = txtNombres.getText().toString().trim();
                String apellidos = txtApellidos.getText().toString().trim();
                String celular = txtCelular.getText().toString().trim();
                String correo = txtCorreo.getText().toString().trim();
                String password = txtPassword.getText().toString().trim();

                if (!nombres.isEmpty() && !apellidos.isEmpty() && !celular.isEmpty() && !correo.isEmpty() && !password.isEmpty()) {
                    Constant.hideKeyboard(RegisterActivity.this);
                    registrarUsuario();
                } else {

                    if (nombres.isEmpty())
                    txtNombres.setError("Debes ingresar almenos un nombre");

                    if (apellidos.isEmpty())
                    txtApellidos.setError("Debes ingresar almenos un apellido");

                    if (celular.isEmpty())
                    txtCelular.setError("Debes ingresar el celular");

                    if (correo.isEmpty())
                    txtCorreo.setError("Debes ingresar el correo");

                    if (password.isEmpty())
                    txtPassword.setError("Debes ingresar la contraseña");

                }
            }
        });

        btnIniciarSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void registrarUsuario() {

        loadingProgressBar.setVisibility(View.VISIBLE);
        btnRegistro.setVisibility(View.GONE);

        String url = Constant.URL_REGISTRAR_USUARIO;

        Map<String, String> params = new HashMap<String, String>();
        params.put("first_name", txtNombres.getText().toString().trim());
        params.put("last_name", txtApellidos.getText().toString().trim());
        params.put("email", txtCorreo.getText().toString().trim());
        params.put("phone", txtCelular.getText().toString().trim());
        params.put("password", txtPassword.getText().toString().trim());

        JSONObject parameters = new JSONObject(params);

        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    String status = response.getString("status");
                    if (status.equals("200")) {
                        txtNombres.setText("");
                        txtApellidos.setText("");
                        txtCorreo.setText("");
                        txtCelular.setText("");
                        txtPassword.setText("");
                        loadingProgressBar.setVisibility(View.GONE);
                        btnRegistro.setVisibility(View.VISIBLE);
                        redirectToLogin();
                    } else if (status.equals("409")) {
                        Constant.showAlertDialogButtonClicked("Aviso !",response.getString("message"), RegisterActivity.this);
                        loadingProgressBar.setVisibility(View.GONE);
                        btnRegistro.setVisibility(View.VISIBLE);
                    } else if (status.equals("404")) {
                        Constant.showAlertDialogButtonClicked("Aviso !",response.getString("message"), RegisterActivity.this);
                        loadingProgressBar.setVisibility(View.GONE);
                        btnRegistro.setVisibility(View.VISIBLE);
                    }

                    Log.i("Status response: ", response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();;
                    Toast.makeText(RegisterActivity.this, "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
                    loadingProgressBar.setVisibility(View.GONE);
                    btnRegistro.setVisibility(View.VISIBLE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                Toast.makeText(RegisterActivity.this, "Error: " + error.toString(), Toast.LENGTH_SHORT).show();
                Log.i("ERROR", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }


    public void redirectToLogin() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle("Aviso !");
        builder.setMessage("Usuario registrado correctamente");
        // add a button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}