package com.example.appgym.ui.login;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appgym.Constant;
import com.example.appgym.MainActivity;
import com.example.appgym.R;
import com.example.appgym.manager.SessionManager;
import com.example.appgym.ui.register.RegisterActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    SessionManager sessionManager;
    EditText usernameText;
    EditText passwordText;
    Button loginButton;
    ProgressBar loadingProgressBar;
    TextView txtRedirectRegister;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManager = new SessionManager(this);

        usernameText = findViewById(R.id.username);
        passwordText = findViewById(R.id.password);
        loginButton = findViewById(R.id.login);
        txtRedirectRegister = findViewById(R.id.txtRedirectRegister);

        loadingProgressBar = findViewById(R.id.loading);

        //
        usernameText.setText("bruce1dev@gmail.com");
        passwordText.setText("123456");
        //

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameText.getText().toString().trim();
                String password = passwordText.getText().toString().trim();

                if (!username.isEmpty() && !password.isEmpty()) {
                    Login(username, password);
                } else {

                    if (username.isEmpty())
                    usernameText.setError("Debes ingresar el correo");

                    if (password.isEmpty())
                    passwordText.setError("Debes ingresar la contraseña");

                }
            }
        });

        txtRedirectRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });
    }

    private void Login(final String username, final String password)
    {
        loadingProgressBar.setVisibility(View.VISIBLE);
        loginButton.setVisibility(View.GONE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");

                    if (status.equals("200")) {

                        JSONObject userData = jsonObject.getJSONObject("user_data");

                        String userId = userData.getString("id");
                        String nombres = userData.getString("first_name");
                        String apellidos = userData.getString("last_name");
                        String correo = userData.getString("email");
                        String celular = userData.getString("phone");
                        String activo = userData.getString("active");
                        String rol = userData.getString("rol");
                        String foto = userData.getString("photo");

                        //create session
                        sessionManager.createSession(userId, nombres, apellidos, correo, celular, activo, rol, foto);

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);

                        loadingProgressBar.setVisibility(View.GONE);
                    } else if(status.equals("409")) {
                        Constant.showAlertDialogButtonClicked("Aviso !", jsonObject.getString("user_data"), LoginActivity.this);
                        loadingProgressBar.setVisibility(View.GONE);
                        loginButton.setVisibility(View.VISIBLE);
                    } else {
                        Constant.showAlertDialogButtonClicked("Aviso !", jsonObject.getString("user_data"), LoginActivity.this);
                        loadingProgressBar.setVisibility(View.GONE);
                        loginButton.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();;
                    Toast.makeText(LoginActivity.this, "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
                    loadingProgressBar.setVisibility(View.GONE);
                    loginButton.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingProgressBar.setVisibility(View.GONE);
                loginButton.setVisibility(View.VISIBLE);
                Toast.makeText(LoginActivity.this, "Error: " + error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", username);
                params.put("password", password);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}