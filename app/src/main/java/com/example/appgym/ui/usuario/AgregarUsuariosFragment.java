package com.example.appgym.ui.usuario;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.appgym.Constant;
import com.example.appgym.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AgregarUsuariosFragment extends Fragment {

    EditText txtNombres, txtApellidos, txtCelular, txtCorreo, txtPassword;
    Switch swtActivo, swtAdmin;
    Button btnRegistro;

    String activeValue = "0";
    String rolValue = "2";

    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_agregar_usuarios, container, false);

        txtNombres = view.findViewById(R.id.txtNombres);
        txtApellidos = view.findViewById(R.id.txtApellidos);
        txtCelular = view.findViewById(R.id.txtCelular);
        txtCorreo = view.findViewById(R.id.txtCorreo);
        txtPassword = view.findViewById(R.id.txtPassword);
        swtActivo = (Switch) view.findViewById(R.id.idActivo);
        swtAdmin = (Switch) view.findViewById(R.id.idAdministrador);
        btnRegistro = view.findViewById(R.id.btnInha);

        requestQueue = Volley.newRequestQueue(getContext());

        swtActivo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    activeValue = "1";
                }
            }
        });

        swtAdmin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rolValue = "1";
                }
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarUsuario();
            }
        });

        return view;
    }

    public void registrarUsuario()
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("first_name", txtNombres.getText().toString());
        params.put("last_name", txtApellidos.getText().toString());
        params.put("email", txtCorreo.getText().toString());
        params.put("phone", txtCelular.getText().toString());
        params.put("password", txtPassword.getText().toString());
        params.put("active", activeValue);
        params.put("rol", rolValue);

        JSONObject parameters = new JSONObject(params);

        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constant.URL_REGISTRAR_USUARIO, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Constant.showAlertDialogButtonClicked("Bien !","Usuario creado exitosamente.", getContext());
                Log.i("Status response: ", response.toString());

                txtNombres.setText("");
                txtApellidos.setText("");
                txtCorreo.setText("");
                txtCelular.setText("");
                txtPassword.setText("");
                swtActivo.setChecked(false);
                swtAdmin.setChecked(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                Constant.showAlertDialogButtonClicked("Ocurrió un error !","No se pudo crear el usuario.", getContext());
                Log.i("ERROR", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }
}
