package com.example.appgym.ui.inicio;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.appgym.R;
import com.example.appgym.manager.SessionManager;
import com.example.appgym.model.Usuario;

public class InicioFragment extends Fragment {

    SessionManager sessionManager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_inicio, container, false);

        sessionManager = new SessionManager(getContext());
        Usuario user = sessionManager.getUserDetails();

        final TextView textView = root.findViewById(R.id.text_home);

        textView.setText("Bienvenido " + user.getNombres() + " !");
        return root;
    }
}