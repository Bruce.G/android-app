package com.example.appgym.ui.perfil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appgym.Constant;
import com.example.appgym.MainActivity;
import com.example.appgym.R;
import com.example.appgym.manager.SessionManager;
import com.example.appgym.model.Usuario;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PerfilFragment extends Fragment {

    EditText txtNombres, txtApellidos, txtCelular, txtPassword;
    Button btnGuardarCambios, btnUploadImg;
    ImageView profileImg;

    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    SessionManager sessionManager;
    Usuario usuario;
    Bitmap bitmap;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_perfil, container, false);

        requestQueue = Volley.newRequestQueue(getContext());

        txtNombres = root.findViewById(R.id.txtNombres);
        txtApellidos = root.findViewById(R.id.txtApellidos);
        txtCelular = root.findViewById(R.id.txtCelular);
        txtPassword = root.findViewById(R.id.txtPassword);
        btnGuardarCambios = root.findViewById(R.id.btnGuardarCambios);

        profileImg = root.findViewById(R.id.profileImg);

        btnUploadImg = root.findViewById(R.id.btnUploadImg);

        sessionManager = new SessionManager(getContext());
        usuario = sessionManager.getUserDetails();

        txtNombres.setText(usuario.getNombres());
        txtApellidos.setText(usuario.getApellidos());
        txtCelular.setText(usuario.getCelular());
        Picasso.with(getContext()).load(usuario.getFoto()).placeholder(getContext().getResources().getDrawable(R.mipmap.ic_launcher)).fit().into(profileImg);

        btnGuardarCambios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombres = txtNombres.getText().toString().trim();
                String apellidos = txtApellidos.getText().toString().trim();
                String celular = txtCelular.getText().toString().trim();

                if (!nombres.isEmpty() && !apellidos.isEmpty() && !celular.isEmpty()) {
                    GuardarCambios();
                } else {

                    if (nombres.isEmpty())
                        txtNombres.setError("Debes ingresar un nombre");

                    if (apellidos.isEmpty())
                        txtApellidos.setError("Debes ingresar un apellido");

                    if (celular.isEmpty())
                        txtApellidos.setError("Debes ingresar un celular");

                }
            }
        });

        btnUploadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseFile();
            }
        });

        return root;
    }

    public void GuardarCambios() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("first_name", txtNombres.getText().toString().trim());
        params.put("last_name", txtApellidos.getText().toString().trim());
        params.put("phone", txtCelular.getText().toString().trim());
        params.put("password", txtPassword.getText().toString().trim());

        JSONObject parameters = new JSONObject(params);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, Constant.URL_ACTUALIZAR_USUARIO + usuario.getId(), parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    int status = response.getInt("status");

                    if (status == 200) {
                        Constant.showAlertDialogButtonClicked("Bien !","Perfil actualizado.", getContext());
                        sessionManager.updateSession(txtNombres.getText().toString(), txtApellidos.getText().toString(), txtCelular.getText().toString());
                        txtPassword.setText("");
                    }  else if (status == 404) {
                        Constant.showAlertDialogButtonClicked("Aviso !","No se pudo actualizar el perfil.", getContext());
                    }
                    Log.i("Status response: ", response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                Constant.showAlertDialogButtonClicked("Ocurrió un error !","No se pudo actualizar el perfil.", getContext());
                Log.i("ERROR", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    public void chooseFile()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccionar imagen"), 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                profileImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            UploadImage(String.valueOf(usuario.getId()), getStringImage(bitmap));
        }
    }

    private void UploadImage(final String id, final String photo) {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Subiendo...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.UPLOAD_IMAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.i("RESPONSE", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if (status.equals("200")) {
                        sessionManager.updatePhoto(jsonObject.getString("foto"));
                        Toast.makeText(getContext(), "Hecho! " + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Intentelo de nuevo! " + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "Intentelo de nuevo! " + error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", id);
                params.put("photo", photo);
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }

    private String getStringImage(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

        byte[] imageByteArray = byteArrayOutputStream.toByteArray();
        String encodeImage = Base64.encodeToString(imageByteArray, Base64.DEFAULT);

        return encodeImage;
    }
}