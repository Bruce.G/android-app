package com.example.appgym.ui.usuario;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.appgym.Constant;
import com.example.appgym.R;
import com.example.appgym.adapter.UsuarioAdapter;
import com.example.appgym.manager.SessionManager;
import com.example.appgym.model.Usuario;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListaUsuariosFragment extends Fragment {

    RecyclerView recyclerView;
    ArrayList<Usuario> listaUsuarios;

    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lista_usuarios, container, false);

        listaUsuarios = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        requestQueue = Volley.newRequestQueue(getContext());
        cargarUsuarios();

        return view;
    }

    public void cargarUsuarios()
    {
        String url = Constant.URL_LISTA_USUARIOS;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Usuario usuario = null;

                try {
                    JSONArray jsonArray = response.optJSONArray("all_users");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        usuario = new Usuario();

                        JSONObject jsonObject = null;
                        jsonObject = jsonArray.getJSONObject(i);

                        usuario.setId(Integer.parseInt(jsonObject.optString("id")));
                        usuario.setNombres(jsonObject.optString("first_name"));
                        usuario.setApellidos(jsonObject.optString("last_name"));
                        usuario.setCorreo(jsonObject.optString("email"));
                        usuario.setCelular(jsonObject.optString("phone"));
                        usuario.setActivo(jsonObject.optString("active"));
                        usuario.setRol(jsonObject.optString("rol"));
                        usuario.setFoto(jsonObject.optString("photo"));

                        listaUsuarios.add(usuario);

                        UsuarioAdapter adapter = new UsuarioAdapter(listaUsuarios);
                        recyclerView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i("ERROR", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }
}
