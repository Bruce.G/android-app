package com.example.appgym.ui.usuario;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.appgym.Constant;
import com.example.appgym.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BuscarEliminarUsuariosFragment extends Fragment {

    EditText txtId, txtNombres, txtApellidos, txtCelular, txtCorreo;
    Switch swtActivo, swdtRol;
    Button btnBuscar, btnInha, btnLimpiar, btnEliminar, btnAdmin;

    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_buscar_eliminar_usuarios, container, false);

        txtId = view.findViewById(R.id.txtId);
        txtNombres = view.findViewById(R.id.txtNombres);
        txtApellidos = view.findViewById(R.id.txtApellidos);
        txtCelular = view.findViewById(R.id.txtCelular);
        txtCorreo = view.findViewById(R.id.txtCorreo);
        swtActivo = (Switch) view.findViewById(R.id.idActivo);
        swdtRol = (Switch) view.findViewById(R.id.idAdmin);
        btnBuscar = view.findViewById(R.id.btnBuscar);
        btnLimpiar = view.findViewById(R.id.btnLimpiar);
        btnInha = view.findViewById(R.id.btnInha);
        btnAdmin = view.findViewById(R.id.btnSetAdmin);
        btnEliminar = view.findViewById(R.id.btnEliminar);

        requestQueue = Volley.newRequestQueue(getContext());


        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eliminarUsuario();
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buscarUsuario();
                Constant.hideKeyboard(getActivity());
            }
        });

        btnInha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inhaUsuario();
            }
        });

        btnAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAdmin();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarInputs();
            }
        });

        return view;
    }

    public void limpiarInputs()
    {
        txtId.setText("");
        txtNombres.setText("");
        txtApellidos.setText("");
        txtCorreo.setText("");
        txtCelular.setText("");
        swtActivo.setChecked(false);
        swdtRol.setChecked(false);
    }

    public void buscarUsuario()
    {
        String idUsuario = txtId.getText().toString();
        String url = Constant.URL_BUSCAR_USUARIO + idUsuario;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    int status = response.getInt("status");
                    if (status == 200) {
                        JSONObject userData = response.getJSONObject("user_data");
                        txtNombres.setText(userData.getString("first_name"));
                        txtApellidos.setText(userData.getString("last_name"));
                        txtCorreo.setText(userData.getString("email"));
                        txtCelular.setText(userData.getString("phone"));

                        if (userData.getString("active").equals("1")) {
                            swtActivo.setChecked(true);
                        }

                        if (userData.getString("rol").equals("1")) {
                            swdtRol.setChecked(true);
                        }

                    }  else if (status == 404) {
                        txtNombres.setText("");
                        txtApellidos.setText("");
                        txtCorreo.setText("");
                        txtCelular.setText("");
                        swtActivo.setChecked(false);
                        swdtRol.setChecked(false);
                        Constant.showAlertDialogButtonClicked("Aviso !","Usuario no encontrado.", getContext());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                Constant.showAlertDialogButtonClicked("Ocurrió un error !","No se pudo buscar al usuario.", getContext());
                Log.i("ERROR", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    public void setAdmin()
    {
        String idUsuario = txtId.getText().toString();
        String url = Constant.URL_ACTUALIZAR_USUARIO + idUsuario;

        Map<String, String> params = new HashMap<String, String>();
        String rolValue = "1";
        if (swdtRol.isChecked()) {
            rolValue = "2";
        }

        params.put("rol", rolValue);
        JSONObject parameters = new JSONObject(params);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 200) {

                        if (swdtRol.isChecked()) {
                            swdtRol.setChecked(false);
                        } else {
                            swdtRol.setChecked(true);
                        }

                        Constant.showAlertDialogButtonClicked("Aviso !",message, getContext());
                    }  else if (status == 404) {
                        Constant.showAlertDialogButtonClicked("Aviso !",message, getContext());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                Constant.showAlertDialogButtonClicked("Ocurrió un error !","No se pudo actualizar al usuario.", getContext());
                Log.i("ERROR", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    public void inhaUsuario()
    {
        String idUsuario = txtId.getText().toString();
        String url = Constant.URL_ACTUALIZAR_USUARIO + idUsuario;

        Map<String, String> params = new HashMap<String, String>();
        String activeValue = "1";
        if (swtActivo.isChecked()) {
            activeValue = "0";
        }
        params.put("active", activeValue);
        JSONObject parameters = new JSONObject(params);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 200) {
                        if (swtActivo.isChecked()) {
                            swtActivo.setChecked(false);
                        } else {
                            swtActivo.setChecked(true);
                        }

                        Constant.showAlertDialogButtonClicked("Aviso !",message, getContext());
                    }  else if (status == 404) {
                        Constant.showAlertDialogButtonClicked("Aviso !",message, getContext());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                Constant.showAlertDialogButtonClicked("Ocurrió un error !","No se pudo actualizar al usuario.", getContext());
                Log.i("ERROR", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    public void eliminarUsuario()
    {
        String idUsuario = txtId.getText().toString();
        String url = Constant.URL_ELIMINAR_USUARIO + idUsuario;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 200) {
                        Constant.showAlertDialogButtonClicked("Aviso !",message, getContext());
                        limpiarInputs();
                    }  else if (status == 404) {
                        Constant.showAlertDialogButtonClicked("Aviso !",message, getContext());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                Constant.showAlertDialogButtonClicked("Ocurrió un error !","No se pudo eliminar al usuario.", getContext());
                Log.i("ERROR", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }
}
