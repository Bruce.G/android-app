package com.example.appgym.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appgym.R;
import com.example.appgym.manager.SessionManager;
import com.example.appgym.model.Usuario;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.UsuarioViewHolder> {

    ArrayList<Usuario> listaUsuarios;
    Context context;

    public UsuarioAdapter(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    @NonNull
    @Override
    public UsuarioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_usuario_list, null, false);
        return new UsuarioViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsuarioViewHolder holder, int position) {
        String textActivo = "";
        if (listaUsuarios.get(position).getActivo().equals("1")) {
            textActivo = "Si";
        } else {
            textActivo = "No";
        }
        holder.txtNombres.setText(listaUsuarios.get(position).getNombres() + " " + listaUsuarios.get(position).getApellidos());
        holder.txtId.setText("Id: " + listaUsuarios.get(position).getId());
        holder.txtCelular.setText("Celular: " + listaUsuarios.get(position).getCelular());
        holder.txtCorreo.setText("Correo: " + listaUsuarios.get(position).getCorreo());
        holder.txtActivo.setText("Activo: " + textActivo);

        Picasso.with(holder.userImage.getContext())
                .load(listaUsuarios.get(position).getFoto())
                .placeholder(holder.userImage.getContext().getResources().getDrawable(R.mipmap.ic_launcher))
                .fit().into(holder.userImage);
    }

    @Override
    public int getItemCount() {
        return listaUsuarios.size();
    }

    public class UsuarioViewHolder extends RecyclerView.ViewHolder {

        TextView txtNombres, txtCelular, txtCorreo, txtActivo, txtId;
        ImageView userImage;

        public UsuarioViewHolder(@NonNull View itemView) {
            super(itemView);
            txtId = (TextView) itemView.findViewById(R.id.txtId);
            txtNombres = (TextView) itemView.findViewById(R.id.txtNombres);
            txtCelular = (TextView) itemView.findViewById(R.id.txtCelular);
            txtCorreo = (TextView) itemView.findViewById(R.id.txtCorreo);
            txtActivo = (TextView) itemView.findViewById(R.id.txtActivo);
            userImage = (ImageView) itemView.findViewById(R.id.user_image);
        }
    }
}
