package com.example.appgym.manager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.appgym.MainActivity;
import com.example.appgym.model.Usuario;
import com.example.appgym.ui.login.LoginActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class SessionManager {
    SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public Context context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "LOGIN";
    private static final String LOGIN = "IS_LOGIN";

    public static final String ID = "ID";
    public static final String NOMBRES = "NOMBRES";
    public static final String APELLIDOS = "APELLIDOS";
    public static final String CORREO = "CORREO";
    public static final String CELULAR = "CELULAR";
    public static final String ACTIVO = "ACTIVO";
    public static final String ROL = "ROL";
    public static final String FOTO = "FOTO";

    public SessionManager(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void createSession(String id, String nombres, String apellidos, String correo, String celular, String activo, String rol, String foto){

        editor.putBoolean(LOGIN, true);
        editor.putString(ID, id);
        editor.putString(NOMBRES, nombres);
        editor.putString(APELLIDOS, apellidos);
        editor.putString(CORREO, correo);
        editor.putString(CELULAR, celular);
        editor.putString(ACTIVO, activo);
        editor.putString(ROL, rol);
        editor.putString(FOTO, foto);
        editor.apply();

    }

    public void updateSession(String nombres, String apellidos, String celular){
        editor.putString(NOMBRES, nombres);
        editor.putString(APELLIDOS, apellidos);
        editor.putString(CELULAR, celular);
        editor.apply();
    }

    public void updatePhoto(String foto) {
        editor.putString(FOTO, foto);
        editor.apply();
    }

    public boolean isLoggin(){
        return sharedPreferences.getBoolean(LOGIN, false);
    }

    public void checkLogin(){
        if (!this.isLoggin()){
            Intent i = new Intent(context, LoginActivity.class);
            context.startActivity(i);
            ((MainActivity) context).finish();
        }
    }

    /*public HashMap<String, String> getUserDetail(){
        HashMap<String, String> user = new HashMap<>();
        user.put(ID, sharedPreferences.getString(ID, null));
        user.put(NOMBRES, sharedPreferences.getString(NOMBRES, null));
        user.put(APELLIDOS, sharedPreferences.getString(APELLIDOS, null));
        user.put(CORREO, sharedPreferences.getString(CORREO, null));
        user.put(CELULAR, sharedPreferences.getString(CELULAR, null));
        user.put(ACTIVO, sharedPreferences.getString(ACTIVO, null));

        return user;
    }*/

    public Usuario getUserDetails(){
        Usuario usuario = new Usuario();
        usuario.setId(Integer.parseInt(sharedPreferences.getString(ID, null)));
        usuario.setNombres(sharedPreferences.getString(NOMBRES, null));
        usuario.setApellidos(sharedPreferences.getString(APELLIDOS, null));
        usuario.setCorreo(sharedPreferences.getString(CORREO, null));
        usuario.setCelular(sharedPreferences.getString(CELULAR, null));
        usuario.setActivo(sharedPreferences.getString(ACTIVO, null));
        usuario.setRol(sharedPreferences.getString(ROL, null));
        usuario.setFoto(sharedPreferences.getString(FOTO, null));
        return usuario;
    }

    public void logout(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(context, LoginActivity.class);
        context.startActivity(i);
        ((MainActivity) context).finish();
    }
}
