package com.example.appgym;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;

import com.example.appgym.ui.login.LoginActivity;
import com.example.appgym.ui.register.RegisterActivity;

public class Constant {

    private static String API_URL = "https://project-android-gfv.herokuapp.com";
    public static final String URL_REGISTRAR_USUARIO = API_URL + "/user/register";
    public static final String URL_LISTA_USUARIOS = API_URL + "/user/getAll";
    public static final String URL_BUSCAR_USUARIO = API_URL + "/user/findById/";
    public static final String URL_ELIMINAR_USUARIO = API_URL + "/user/delete/";
    public static final String URL_ACTUALIZAR_USUARIO = API_URL + "/user/update/";
    public static final String LOGIN_URL = API_URL + "/login";
    public static final String UPLOAD_IMAGE = API_URL + "/upload_image";

    public static final String CHAT_SOCKETURI = "https://project-android-chat.herokuapp.com/";

    // funciones
    public static void showAlertDialogButtonClicked(String title, String message, Context context) {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);

        // add a button
        builder.setPositiveButton("OK", null);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // ocultar teclado
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow((activity).getWindow()
                .getCurrentFocus().getWindowToken(), 0);
    }
}
